FROM node:16-alpine as build
RUN mkdir /build/
WORKDIR /build
ADD . /build/
RUN  export NODE_OPTIONS=--max_old_space_size=8192 && \
	 cd dv-post-handler && npm ci && cd .. && \
	 cd keycloak-user-sync && npm ci && cd .. && \
	 cd dv-post-handler && npm run build && cd .. && \
	 cd keycloak-user-sync && npm run build

FROM directus/directus:9.18.1

ENV AUTH_PROVIDERS="keycloak"
ENV AUTH_KEYCLOAK_DRIVER="openid"
ENV AUTH_KEYCLOAK_ALLOW_PUBLIC_REGISTRATION="true"

ENV CORS_ENABLED=true

# The env variable DB_CLIENT is not picked up at CMD runtime otherwise, causing the npx call to not found it properly
RUN unset DB_CLIENT && unset DB_FILENAME

# Disable warnings about npm updates
RUN npm config set update-notifier false

COPY --from=build /build/dv-post-handler/dist /directus/extensions/hooks/dv-post-handler/
COPY --from=build /build/dv-post-handler/templates /directus/extensions/templates/

COPY --from=build /build/keycloak-user-sync/dist /directus/extensions/hooks/keycloak-user-sync/

COPY schema/snapshot.yaml /directus/dv-snapshot.yaml

CMD npx directus bootstrap && (npx directus schema apply --yes /directus/dv-snapshot.yaml || exit 0) && npx directus start
