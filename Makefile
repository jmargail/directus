.PHONY: all clean install build image

all: build image

clean:
	rm -r keycloak-user-sync/dist/
	rm -r keycloak-user-sync/node_modules/
	rm -r dv-post-handler/dist/
	rm -r dv-post-handler/node_modules/

install:
	cd dv-post-handler && npm install
	cd keycloak-user-sync && npm install

build:
	cd dv-post-handler && npm run build
	cd keycloak-user-sync && npm run build

image:
	docker build -t directus-keycloak .
