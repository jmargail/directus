# directus

This Project contains the [directus](https://directus.io/) extensions used by the portal `https://portal.digital-vital.eu/`. The information displayed in the modules
`Anlaufstellen, Vorsorge und mehr`, `Neuigkeiten` and `fit-gesund` are created using `Directus` with these extensions.

These extensions include:

- `dv-post-handler`: it adds a flow for publication of posts. Creates a workflow based on different roles
- `keycloak-user-sync`: adds the authentication flow using [KeyCloak](https://www.keycloak.org/)


## Requirements

In order to configure these extensions, besides a Directus (https://directus.io/) installation, you need:
- a running https://www.keycloak.org/ 
- a mailserver: to send the notifications about new posts


## Publishing flow

The extension `Post-Handler` changes the publication flow. Instead of having the posts directly available, it adds an approval flow:

![Directus Publishing Flow](./docs/directus_flow.png)


## Roles

- `Reporter`: Can create posts but not publish
- `Editor`: Can create and publish own posts
- `ChiefEditor`: Can create, edit and publish own posts and posts from others
- `HelpEditor`: Can create posts of type "Help" and publish them

These roles need to be added in KeyCloak


## Authors and acknowledgment

- [Matthias Gerbershagen](https://gitlab.com/mgerbersh)

## License
MIT License

Digital Vital
Copyright (c) 2023 Fraunhofer IESE

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

## Project Status
The project has been developed using a private `GitLab` instance. The main development phase ended on December 2022 and the project is currently in the maintenance mode. 