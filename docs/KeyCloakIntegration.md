# Directus Keycloak Client

Integration of Keycloak with Directus is achived by two means:
- activating the openid authentication in directus
- adding a hook extension to directus to sync the user roles upon login


## Requirements

Requires a confidential client in keycloak
Service Accounts Enabled Option needs to be on
The service account requires the following client roles for the current realm (choose <realm-name>-realm as client): view-clients, view-realm, view-users
Role Mapping Directus vs. Keycloak Roles
For each Keycloak Role a corresponding Directus Role has to exist. Directus Roles have to be created manually by a directus admin.
Keycloak Roles that should be mapped to Directus Roles have to be created in the directus client as Client Roles
Keycloak Roles that should be mapped to Directus Roles have to be prefixed by Directus_ (this prefix will be removed before doing the role matching in directus)
Remember: Directus only allows to assign at most one role to each user!
Directus OpenId Auth Flow


![Directus Authentication Flow](./directus-auth-flow.png)

## Directus Extension

The extension is written in typescript and uses the directus extension sdk
The extension registers a hook at the auth.login action of directus (therefore roles are assigned/updated upon each login in directus
A role change requires a logout/login cycle of the user to become active!
The hook is executed before the admin ui page is shown to the user
Be careful: doing a long running synchronous operation in the hook causes the login the be slow for the user!
The extension retrieves the client role mapping of the keycloak user for the logged in user by making the necessary requests at the keycloak rest api (https://www.keycloak.org/docs-api/16.0/rest-api/index.html)
The directus role is updated and stored for the  logged in user in directus
