import { defineHook } from '@directus/extensions-sdk';
import { Accountability, Collection, Item, PrimaryKey, Query, User } from '@directus/shared/types';
import { Logger } from 'pino';

type TSchema = Record<string, any> | null;
type TAccountability = Accountability | null;

const ADMINISTRATOR = 'Administrator';
const EDITOR = 'Editor';
const CHIEF_EDITOR = 'ChiefEditor';

const USER_PORTAL_VARIANT_IDENTIFIER = 'user_portal_variant_identifier';

enum PostStatus {
	DRAFT = 'draft',
	PUBLISHED = 'published'
}

function isNil<T>(t: T | undefined | null): boolean {
	return t === undefined || t === null;
}

const getRoleIdByName = async (rolesService: any, name: string): Promise<PrimaryKey | undefined> => {
	const roleQuery: Query = { filter: { 'name': { _eq: name } } };
	const roleKeys = await rolesService.getKeysByQuery(roleQuery);
	return roleKeys.at(0);
}

const getUsersWithRole = async (usersService: any, roleId: PrimaryKey): Promise<Array<User>> => {
	const userQuery: Query = { fields: ['id', 'email'], filter: { 'role': { _eq: roleId } } };
	const users = await usersService.readByQuery(userQuery) as Array<User>;
	return users;
}

const getUserIdsWithPortalVariant = async (userPortalVariantService: any, portalVariantIdentifier: string): Promise<string[]> => {
	const query: Query = { filter: { 'portal_variant_identifier': { _eq: portalVariantIdentifier } } };
	const entries = await userPortalVariantService.readByQuery(query) as Array<Item>;
	return entries.map((e) => e.user_id);
}

const getUsersWithRoleAndPortalVariant = async (usersService: any, userPortalVariantService: any, roleId: PrimaryKey, portalVariantIdentifier: string): Promise<Array<User>> => {
	const usersWithRole = await getUsersWithRole(usersService, roleId);
	const portalVariantUserIds  = await getUserIdsWithPortalVariant(userPortalVariantService, portalVariantIdentifier);
	const portalVariantUserIdSet = new Set(portalVariantUserIds);
	return usersWithRole.filter((u) => portalVariantUserIdSet.has(u.id));
}

const getPortalVariantIdentifierByUserId = async (userPortalVariantService: any, userId: string): Promise<string | undefined> => {
	const query: Query = { filter: { 'user_id': { _eq: userId } } };
	const entries = await userPortalVariantService.readByQuery(query) as Array<Item>;
	const entry = entries.at(0);
	return entry?.portal_variant_identifier;
}

const sendNotificationEmails = async (env: Record<string, any>, emails: Array<string>, mailService: any, ServiceUnavailableException: any, post: any) => {
	for (const email of emails) {
		try {
			await mailService.send({
				to: email,
				subject: '[Digital Vital] Neuer Beitrag',
				template: {
					name: 'post-created',
					data: {
						post: post,
						url: env["PUBLIC_URL"] + '/admin/content/Post/' + post.id
					},
				},
			});
		} catch (error) {
			throw new ServiceUnavailableException(error);
		}
	}
}

const notifyChiefEditors = async (env: any, logger: Logger, schema: TSchema, exceptions: any, services: any, post: any) => {
	const { ItemsService, MailService, RolesService, UsersService } = services;
	const { ServiceUnavailableException } = exceptions;

	const rolesService = new RolesService({ schema });
	const editorRoleId = await getRoleIdByName(rolesService, EDITOR);
	const chiefEditorRoleId = await getRoleIdByName(rolesService, CHIEF_EDITOR);
	if (editorRoleId === undefined || chiefEditorRoleId === undefined) {
		const roleName = editorRoleId === undefined ? EDITOR : CHIEF_EDITOR;
		logger.warn('Role not found: roleName=%s', roleName);
		return;
	}

	const creatorId = post.user_created;
	const usersService = new UsersService({ schema });
	const creator = await usersService.readOne(creatorId);
	if (creator.role === editorRoleId || creator.role === chiefEditorRoleId) {
		//don't send notification emails if an editor publishes something
		return;
	}

	const mailService = new MailService({ schema });
	const userPortalVariantService = new ItemsService(USER_PORTAL_VARIANT_IDENTIFIER, { schema });

	const portalVariantIdentifier = post.portal_variant_identifier;
	const chiefEditors = await getUsersWithRoleAndPortalVariant(usersService, userPortalVariantService, chiefEditorRoleId, portalVariantIdentifier);
	if (chiefEditors.length > 0) {
		const chiefEditorsEmails = chiefEditors.map((u) => u.email);
		sendNotificationEmails(env, chiefEditorsEmails, mailService, ServiceUnavailableException, post);
	} else {
		const adminRole = await getRoleIdByName(rolesService, ADMINISTRATOR);
		if (adminRole === undefined) {
			logger.warn("Not sending emails, no chiefeditors and no admins found. Check your configuration!");
			return;
		}
		const admins =  await getUsersWithRole(usersService, adminRole);
		const adminEmails = admins.map((u) => u.email);
		sendNotificationEmails(env, adminEmails, mailService, ServiceUnavailableException, post);
	}
	logger.info("Notification emails send for post: postId=%s", post.id);
}

const handleSubCategoryCreate = async (key: PrimaryKey, collection: Collection, schema: TSchema, accountability: TAccountability, logger: Logger, services: any) => {
	if (!accountability) {
		return;
	}
	const { user, role } = accountability;
	if (!user || !role) {
		return;
	}
	const hasAdminRole = await isAdmin(schema, role, services);
	if (hasAdminRole) {
		return;
	}

	const { ItemsService } = services;
	const itemsService = new ItemsService(collection, { schema });
	const userPortalVariantIdService = new ItemsService(USER_PORTAL_VARIANT_IDENTIFIER, { schema });

	const item = await itemsService.readOne(key);

	const portalVariantId = await getPortalVariantIdentifierByUserId(userPortalVariantIdService, user);
	item.portal_variant_identifier = portalVariantId;

	await itemsService.updateOne(key, item);
	logger.info("PortalVariantIdentifier assigned to subcategory: id=%s, portalVariantIdentifier=%s", key, portalVariantId);
}

const handleItemsCreate = async (key: PrimaryKey, collection: Collection, schema: TSchema, logger: Logger, services: any) => {
	const { ItemsService, UsersService } = services;
	const itemsService = new ItemsService(collection, { schema });
	const userPortalVariantIdService = new ItemsService(USER_PORTAL_VARIANT_IDENTIFIER, { schema });
	const usersService = new UsersService({ schema });

	const item = await itemsService.readOne(key);
	const creatorId = item.user_created;
	const creator = await usersService.readOne(creatorId);

	item.dv_user_created = creator.external_identifier;

	const portalVariantId = await getPortalVariantIdentifierByUserId(userPortalVariantIdService, creatorId);
	item.portal_variant_identifier = portalVariantId;

	if (item.status === PostStatus.PUBLISHED) {
		item.date_published = new Date();
	}

	await itemsService.updateOne(key, item);
	logger.info("PortalVariantIdentifier assigned to post: id=%s, portalVariantIdentifier=%s", key, portalVariantId);

	// no chiefeditor notification here, because items update hook is called as well
};

const handleItemsUpdate = async (keys: Array<PrimaryKey>, collection: Collection, schema: TSchema, env: any, exceptions: any, logger: Logger, services: any) => {
	const { ItemsService } = services;
	const itemsService = new ItemsService(collection, { schema });

	const items = await itemsService.readMany(keys) as Array<any>;

	for (const item of items) {
		switch (item.status) {
			case PostStatus.PUBLISHED:
				if (isNil(item.date_published)) {
					item.date_published = new Date();
					await itemsService.updateOne(item.id, item);
				}
				break;
			case PostStatus.DRAFT:
				if (item.submitted) {
					await notifyChiefEditors(env, logger, schema, exceptions, services, item);
				}
				break;
			default:
				break;
		}
	}
};

const isAdmin = async (schema: TSchema, role: string, services: any): Promise<boolean> => {
	const { RolesService } = services;

	const rolesService = new RolesService({ schema });
	const adminRole = await getRoleIdByName(rolesService, ADMINISTRATOR);
	return role === adminRole;
};

const handleItemsQuery = async (query: Query, schema: TSchema, accountability: TAccountability, services: any): Promise<Query> => {
	if (!accountability) {
		return query;
	}
	const { user, role } = accountability;
	if (!user || !role) {
		return query;
	}
	const hasAdminRole = await isAdmin(schema, role, services);
	if (hasAdminRole) {
		return query;
	}
	const { ItemsService } = services;

	const userPortalVariantService = new ItemsService(USER_PORTAL_VARIANT_IDENTIFIER, { schema });
	const portalVariantIdentifier = await getPortalVariantIdentifierByUserId(userPortalVariantService, user);
	const updatedQuery = { ...query };

	if (!query.filter) {
		updatedQuery.filter = {
			portal_variant_identifier: {
				_eq: portalVariantIdentifier
			}
		}
	} else {
		updatedQuery.filter = {
			_and: [
				{
					portal_variant_identifier: {
						_eq: portalVariantIdentifier
					}
				},
				query.filter
			]
		}
	}
	return updatedQuery;
};

export default defineHook(({ action, filter }, { env, exceptions, logger, services } ) => {
	action('Post.items.create', ({ key, collection }, { schema }) => {
		return handleItemsCreate(key, collection, schema, logger, services);
	});
	action('Post.items.update', ({ keys, collection }, { schema }) => {
		return handleItemsUpdate(keys, collection, schema, env, exceptions, logger, services);
	});

	action('NewsArticle.items.create', ({ key, collection }, { schema }) => {
		return handleItemsCreate(key, collection, schema, logger, services);
	});
	action('NewsArticle.items.update', ({ keys, collection }, { schema }) => {
		return handleItemsUpdate(keys, collection, schema, env, exceptions, logger, services);
	});

	action('SubCategory.items.create', ({ key, collection }, { schema, accountability }) => {
		return handleSubCategoryCreate(key, collection, schema, accountability, logger, services);
	});

	filter('items.query', (query, { collection }, { schema, accountability }) => {
		if (collection === "SubCategory" || collection === "Post" || collection === "NewsArticle") {
			return handleItemsQuery(query, schema, accountability, services);
		}
		return query;
	});
});
