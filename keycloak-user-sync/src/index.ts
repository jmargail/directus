import { defineHook } from '@directus/extensions-sdk';
import { PrimaryKey, Query } from '@directus/shared/types';
import axios from 'axios';
import { Logger } from 'pino';

const DIRECTUS_ROLE_PREFIX = 'Directus_';
const KEYCLOAK_PROVIDER = 'keycloak';

const DIRECTUS_ROLES = 'directus_roles';
const DIRECTUS_USERS = 'directus_users';

const USER_PORTAL_VARIANT_IDENTIFIER = 'user_portal_variant_identifier';

interface TKeycloakUser {
	id: string;
	attributes: Record<string, Array<string>>;
	clientRoles: Array<string>;
}

type TEnv = Record<string, any>;
type TSchema = Record<string, any> | null;
type TServices = any;

class HookContext {
	private _serviceCache: Record<string, any>;

	constructor(public env: TEnv, public logger: Logger, private schema: TSchema, private services: TServices) {
		this._serviceCache = new Map();
	}

	public get rolesService() {
		let rolesService = this._serviceCache[DIRECTUS_ROLES];
		if (!rolesService) {
			const { RolesService } = this.services;
			const schema = this.schema;
			rolesService = new RolesService({ schema });
			this._serviceCache[DIRECTUS_ROLES] = rolesService;
		}
		return rolesService;
	}

	public getItemsService(collection: string) {
		let itemsService = this._serviceCache[collection];
		if (!itemsService) {
			const { ItemsService } = this.services;
			const schema = this.schema;
			itemsService = new ItemsService(collection, { schema });
			this._serviceCache[collection] = itemsService;
		}
		return itemsService;
	}

	public get usersService() {
		let usersService = this._serviceCache[DIRECTUS_USERS];
		if (!usersService) {
			const { UsersService } = this.services;
			const schema = this.schema;
			usersService = new UsersService({ schema });
			this._serviceCache[DIRECTUS_USERS] = usersService;
		}
		return usersService;
	}
}

const getKeycloakRequestHeaders = (accessToken: string) => {
	return { 'Authorization': `Bearer ${accessToken}` };
}

const getAccessToken = async (keycloakBaseUrl: string, env: TEnv): Promise<string> => {
	const tokenUrl = keycloakBaseUrl + '/protocol/openid-connect/token';
	const clientId = env['AUTH_KEYCLOAK_CLIENT_ID'];
	const clientSecret = env['AUTH_KEYCLOAK_CLIENT_SECRET'];
	const params = new URLSearchParams();
	params.set('grant_type', 'client_credentials');
	params.set('client_id', clientId);
	params.set('client_secret', clientSecret);
	const response = await axios.post(tokenUrl, params);
	return response.data.access_token;
}

const getClientResourceId = async (keycloakAdminUrl: string, accessToken: string, clientId: string): Promise<string> => {
	const resourceUrl = keycloakAdminUrl + '/clients';
	const response = await axios.get(resourceUrl, { headers: getKeycloakRequestHeaders(accessToken) });
	const clients = response.data as Array<any>;
	const client = clients.find(client => client.clientId === clientId);
	if (client === undefined) {
		throw new Error('Client not found');
	}
	return client.id;
}

const getClientRoles = async (keycloakAdminUrl: string, accessToken: string, clientResourceId: string, keycloakUserId: string): Promise<Array<string>> => {
	const resourceUrl = keycloakAdminUrl + '/users/' + keycloakUserId + '/role-mappings/clients/' + clientResourceId + '/composite';
	const response = await axios.get(resourceUrl, { headers: getKeycloakRequestHeaders(accessToken) });
	const clientRoleMappings = response.data as Array<any>;
	return clientRoleMappings.map(clientRoleMapping => clientRoleMapping.name);
}

const getKeycloakUserAttributes = async (keycloakAdminUrl: string, accessToken: string, keycloakUserId: string): Promise<Record<string, Array<string>>> => {
	const resourceUrl = keycloakAdminUrl + '/users/' + keycloakUserId;
	const response = await axios.get(resourceUrl, { headers: getKeycloakRequestHeaders(accessToken) });
	const user = response.data as Record<string, any>;
	return user.attributes;
}

const getKeycloakUser = async (keycloakUserId: string, context: HookContext): Promise<TKeycloakUser> => {
	const { env } = context;
	const keycloakBaseUrl = env['AUTH_KEYCLOAK_ISSUER_URL'].replaceAll('/.well-known/openid-configuration', '');
	const accessToken = await getAccessToken(keycloakBaseUrl, env);
	const keycloakAdminUrl = keycloakBaseUrl.replace('realms', 'admin/realms');
	const attributes = await getKeycloakUserAttributes(keycloakAdminUrl, accessToken, keycloakUserId);
	const clientId = env['AUTH_KEYCLOAK_CLIENT_ID'];
	const clientResourceId = await getClientResourceId(keycloakAdminUrl, accessToken, clientId);
	const clientRoles = await getClientRoles(keycloakAdminUrl, accessToken, clientResourceId, keycloakUserId);
	return {
		id: keycloakUserId,
		attributes,
		clientRoles,
	}
}

const getRoleId = async (rolesService: any, roleName: string): Promise<string | undefined> => {
	const query: Query = { filter: { 'name': { _eq: roleName } } };
	const roleKeys = await rolesService.getKeysByQuery(query);
	return roleKeys.at(0);
}

const persistUser = async (userId: string, userEntity: any, context: HookContext): Promise<void> => {
	const { usersService } = context;
	// delete some things that can not be changed
	delete userEntity.tfa_secret;
	delete userEntity.provider;
	delete userEntity.external_identifier;
	await usersService.updateOne(userId, userEntity);
}

const syncKeycloakRole = async (userId: string, userEntity: any, keycloakUser: TKeycloakUser, context: HookContext): Promise<void> => {
	const { logger } = context;
	const keycloakUserRole = keycloakUser.clientRoles.find(r => r.startsWith(DIRECTUS_ROLE_PREFIX));
	if (keycloakUserRole === undefined) {
		logger.warn('User has no roles assigned in keycloak: userId=%s', userId);
		return;
	}
	const roleName = keycloakUserRole.replace(DIRECTUS_ROLE_PREFIX, '');
	const roleId = await getRoleId(context.rolesService, roleName);
	if (roleId === undefined) {
		logger.warn('Role not found in directus: roleName=%s', roleName);
		return;
	}
	userEntity.role = roleId;
	await persistUser(userId, userEntity, context);
	logger.info('User role assigned: userId=%s roleName=%s', userId, roleName);
}

const syncPortalVariantIdentifier = async (userId: string, keycloakUser: TKeycloakUser, context: HookContext): Promise<void> => {
	const { logger } = context;
	const userPortalVariantService = context.getItemsService(USER_PORTAL_VARIANT_IDENTIFIER);
	const portalVariantAttribute = keycloakUser.attributes.portalVariantIdentifier;
	const portalVariantIdentifier = portalVariantAttribute?.at(0);
	if (portalVariantIdentifier === undefined) {
		logger.warn('User has no portal variant identifier assigned: userId=%s', userId);
		return;
	}
	const query: Query = { filter: { 'user_id': { _eq: userId } } };
	const keys = await userPortalVariantService.getKeysByQuery(query) as Array<PrimaryKey>;
	const currentKey = keys.at(0);
	const data = { user_id: userId, portal_variant_identifier: portalVariantIdentifier };
	if (currentKey) {
		await userPortalVariantService.updateOne(currentKey, data);
	} else {
		await userPortalVariantService.createOne(data);
	}
	logger.info('User portal variant identifier assigned: userId=%s portalVariantIdentifier=%s', userId, portalVariantIdentifier);
}

export default defineHook(({ filter }, { env, services, logger }) => {
	filter('auth.login', async (payload, { user, provider }, { schema }) => {
		if (provider !== KEYCLOAK_PROVIDER) {
			return payload;
		}
		const hookContext = new HookContext(env, logger, schema, services);

		const { usersService } = hookContext;
		const userEntity = await usersService.readOne(user);
		const keycloakUser = await getKeycloakUser(userEntity.external_identifier, hookContext);
		await syncKeycloakRole(user, userEntity, keycloakUser, hookContext);
		await syncPortalVariantIdentifier(user, keycloakUser, hookContext);
		return payload;
	});
});
