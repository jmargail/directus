module.exports = {
	async up(knex) {
		await knex.schema.table('NewsArticle', (table) => {
			table.dropForeign('category', 'newsarticle_category_foreign');
		});
	},

	async down(knex) {
		await knex.schema.table('NewsArticle', (table) => {
			table.foreign('category', 'newsarticle_category_foreign').references('Category.id');
		});
	},
};
