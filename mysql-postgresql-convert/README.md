# MySQL PostgreSQL Converter

## Requirements

- python (at least version 3.6.X)
- the mysql dump of the directus database

## Step by Step tutorial

1. Adjust the filenames in the converter script to match your mysql dump file. If you want you can create a separat file with only the create table statements to speed up the processing if you have a huge amount of data
2. Run the converter.py script by issuing the following command: `python converter.py`
3. Open the output file of the script (converted.sql) in your favourite text editor and make shure that the insert statements are in the proper order such that the foreign key constraints are respected. For the directus tables this means the insert statement of the directus_roles table must be before the insert statement of the directus_users table. For our data tables the category insert statement must be before the subcategory insert statement, the helpsection insert statement must be before the help insert statement and the subcategory insert statement must be before the insert post statement
4. Create an empty postgresql database and startup only one directus container. The container will create the directus system tables, an initial admin user and our data tables upon startup
5. Get the id of the initial admin user and shutdown the directus container again.
6. Replace the id of the initial admin user in the converted dump with the new id from the previous step and remove the corresponding insert for the initial admin user from the directus_users insert statement
7. Apply the converted and edited dump to the bootstrapped directus database from step 4. Make shure it is successful! You can use the following command: `psql -U <databaseuser> <converted.sql`
8. Start the directus container back up and scale up to multiple containers if required
