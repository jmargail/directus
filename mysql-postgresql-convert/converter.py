from io import TextIOWrapper
from typing import List

IGNORED_TABLES = ['directus_activity', 'directus_collections', 'directus_fields', 'directus_migrations', 'directus_relations', 'directus_revisions', 'directus_sessions']

table_columns = {}

def get_table_name(statement: str):
    searchendpos = len(statement) -  1
    if statement.startswith('INSERT'):
        searchendpos = statement.find('VALUES')
    startpos = statement.find('"', 0, searchendpos)
    if startpos == -1:
        startpos = statement.find('`', 0, searchendpos)
    startpos += 1
    endpos = statement.find('"', startpos, searchendpos)
    if endpos == -1:
        endpos = statement.find('`', startpos, searchendpos)
    name = statement[startpos:endpos]
    return name

def get_next_statement(text: TextIOWrapper):
    while True:
        line = text.readline()
        if not line:
            return ''
        if line.startswith('/*'):
            continue
        statement = line.replace("\n", " ")
        while not statement.rstrip().endswith(';'):
            line = text.readline()
            if not line:
                break
            if line.startswith("/*"):
                continue
            statement += line.replace("\n", "")
        statement = statement.lstrip()
        return statement


def parse_create(statement: str):
    try:
        name = get_table_name(statement)
        start_pos = statement.index('(') + 1
        end_pos = statement.rindex(')')
        defs = statement[start_pos:end_pos].split(',')
        stripped_defs = [d.strip() for d in defs]
        column_defs = [d for d in stripped_defs if d.startswith('"') or d.startswith('`')]
        column_defs = [d.split(' ')[0:2] for d in column_defs]
        table_columns[name] = column_defs
    except ValueError:
        print("Invalid create table statement found")
        print(statement)

def tokenize_values(statement: str) -> List[str]:
    start_pos = statement.index('(', statement.index('VALUES'))
    end_pos = statement.rindex(')')
    tokens = []
    token_value = statement[start_pos:end_pos]
    in_string = False
    token = ''
    for char in token_value:
        if char == '\'':
            in_string = not in_string
        if char == ',' and not in_string and token.endswith(')'):
            tokens.append(token[:-1])
            token = ''
        else:
            token += char
    tokens.append(token)
    return tokens

def tokenize_value(value: str) -> List[str]:
    tokens = []
    token_value = value
    if value.startswith('('):
        token_value = value[1:]
    if value.startswith('\t('):
        token_value = value[2:]
    in_string = False
    token = ''
    for char in token_value:
        if char == '\'':
            in_string = not in_string
        if char == ',' and not in_string:
            tokens.append(token)
            token = ''
        else:
            token += char
    tokens.append(token)
    return tokens

def convert_insert(statement):
    name = get_table_name(statement)
    column_defs =  table_columns[name]
    start_pos = statement.index('(', statement.index('VALUES'))
    converted_statement = statement[0:start_pos] + '\n'
    converted_statement = converted_statement.replace('`', '"')
    values = tokenize_values(statement)
    converted_values = []
    for v in values:
        value_tokens = tokenize_value(v)
        new_value_tokens = []
        for index,s in enumerate(value_tokens):
            column_def = column_defs[index]
            dataType = column_def[1]
            if dataType == 'tinyint(1)':
                s = s.strip()
                if s == '0':
                    new_value_tokens.append('FALSE')
                elif s == '1':
                    new_value_tokens.append('TRUE')
                else:
                    print("Unknown boolean value: " + s)
            elif dataType == 'json':
                new_value_tokens.append(s.replace('\\"', '"'))
            else:
                new_value_tokens.append(s)
        new_value = '(' + ','.join(new_value_tokens) + ')'
        converted_values.append(new_value)
    converted_statement += ',\n'.join(converted_values)
    return converted_statement

with open('digitalvital_dev_directus.sql', 'r') as schema:
    while True:
        statement = get_next_statement(schema)
        if not statement:
            break
        if statement.startswith('CREATE TABLE'):
            name = get_table_name(statement)
            if name in IGNORED_TABLES:
                continue
            parse_create(statement)

with open("digitalvital_dev_directus.sql", "r") as dump:
    with open("converted.sql", "w") as result:
        result.write("BEGIN;\n")
        result.write('SET CONSTRAINTS ALL DEFERRED;\n')
        while True:
            statement = get_next_statement(dump)
            if not statement:
                break
            if statement.startswith('INSERT'):
                name = get_table_name(statement)
                if name in IGNORED_TABLES:
                    continue
                c = convert_insert(statement)
                result.write(c + ';\n')
        result.write("COMMIT;\n")
